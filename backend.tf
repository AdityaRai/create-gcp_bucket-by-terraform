terraform {
backend "gcs" {
  bucket = "aditya-remote-backend-12345"
  prefix = "terraform/tfstat"
  credentials = "keyfile.json"
  }
}
