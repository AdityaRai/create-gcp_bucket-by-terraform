// Terraform plugin for creating random IDs
resource "random_id" "instance_id" {
  byte_length = 8
}
resource "google_storage_bucket" "bucket" {
#Number of bucket
 count = 1
#Bucket name
 name = "adityabucket-${random_id.instance_id.hex}"
 labels = {
    key = "env" 
    value = "dev"
  }
# Any location of your choice
 location = "europe-west2"
# Any storage_class of your choice
 storage_class = "STANDARD"
 uniform_bucket_level_access = true
}


resource "google_storage_bucket_object" "text" {
  name   = "aditya_file"
  content = "aaabbbccc"
  bucket = "${google_storage_bucket.bucket[0].name}"
}















